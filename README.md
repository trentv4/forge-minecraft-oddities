This repo is for tracking various odd things and shitty behavior you may encounter while developing in Forge. If you'd like to add anything to it, send Trentv4 (me) a PR and I'll try and get to it relatively quickly.

What's 'odd' or 'shitty' behavior? Pretty much anything really complex, or really stupid design decisions in either Forge, the forge to minecraft interfaces, or Minecraft itself. 

Contribution guidelines:

* Pull down the latest changes, to make merging less painful
* Write a brief outline of what's shitty/odd/complex/stupid and what you should do
* If you can, point to an example 
* Submit a PR 



Forge/Minecraft Oddities and shittiness, with tips:
=============================

* If you want to render an item, use ModelLoader or ItemModelMesher. ModelLoader has to be registered in preInit, ItemModelMesher in init.
    * [This commit shows both styles.](https://bitbucket.org/trentv4/advanced-darkness/commits/236bf6d43199f5b996cb3ced7c695602417bd2d5?at=master)



* Capacity default states need to be set like this:
		this.setDefaultState(blockState.getBaseState().withProperty(CAPACITY, 16));

    * [Example of this](https://bitbucket.org/trentv4/advanced-darkness/src/cb6fac6e00af3a57c7fdb36616535845a61867d1/src/main/java/net/trentv/minecraft/darkness/common/BlockGammaAdjuster.java?at=master&fileviewer=file-view-default)



* If you want to dynamicaly recolor objects, you have to implement IBlockColor or IItemColor on another object and register them in Minecraft.getMinecraft().getItemColors or getBlockColors. By the way, this is all client side, so glhf with that.

    * [Example of this here](https://bitbucket.org/trentv4/gases-framework-1.10/src/a080809a13c908e27e5f52a1a2d26476774a1453/src/main/java/net/trentv/gasesframework/common/block/BlockGas.java?at=master&fileviewer=file-view-default) and [here](https://bitbucket.org/trentv4/gases-framework-1.10/src/a080809a13c908e27e5f52a1a2d26476774a1453/src/main/java/net/trentv/gasesframework/client/ClientProxy.java?at=master&fileviewer=file-view-default)


* If you're having issues with a model rendering as "2d" when it should be 3d as an item, make sure your model has it's parent set to 'block/block'


* If you're having issues with model parts rendering out of order, override getBlockLayer in the class and return BlockRenderLayer.CUTOUT
	If you have translucent textures, I'm sorry. You have to place the elements in the json manually so they don't render out of order.